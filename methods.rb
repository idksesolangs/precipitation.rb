require 'discordrb'

def dm_all(people, from, bot, text: "Wuh oh... \nSomething went wrong, and the bot has decided to send this as a coping method. Tell Idk837384 about this!")
  embed ||= Discordrb::Webhooks::Embed.new
  view = Discordrb::Webhooks::View.new

  yield(embed, view) if block_given?

  people.each do |user|
    begin
      bot.user(user).dm("**" + from + "**: " + text) unless text.include? '_ _'
      bot.user(user).dm(text) if text.include? '_ _'
    rescue NoMethodError
      print ""
    end
  end
end

class Message
  def reply(text)
    self.respond(text)
  end
end
