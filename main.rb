require 'youtube-dl.rb'
require 'discordrb'
require 'rufus-scheduler'
load 'methods.rb'
require 'yaml'
bot = Discordrb::Bot.new token: 
testbot = bot
uptime = Discordrb.timestamp(Time.now, :short_datetime)
scheduler = Rufus::Scheduler.new
currentGames = {}
version = 'a_0.2'
version_name = version + ' Jasper'
cmdInfo = {}
prefix = 'prb:'
cmdInfo['uptime'] = {'description' => 'See how long Precipitation.rb has been online.', 'syntax' => "#{prefix}uptime", "disabledservers" => []}
cmdInfo['name'] = {'description' => 'Sets the name for the bot to refer to you as.', 'syntax' => "#{prefix}name (name)", "disabledservers" => []}
cmdInfo['about'] = {'description' => 'Gives information on the bot, as well as credits.', 'syntax' => "#{prefix}about", "disabledservers" => []}
cmdInfo['help'] = {'description' => 'Gets a list of commands, or shows information about a command.', 'syntax' => "#{prefix}help (command)", "disabledservers" => []}
cmdInfo['version'] = {'description' => 'Shows the current bot version.', 'syntax' => "#{prefix}version", "disabledservers" => []}
cmdInfo['ping'] = {'description' => 'Gets the current latency of the bot.', 'syntax' => "#{prefix}ping", "disabledservers" => []}
cmdInfo['pic'] = {'description' => 'Gets the profile picture of yourself or another user.', 'syntax' => "#{prefix}pic (user)", "disabledservers" => []}
cmdInfo['location'] = {'description' => 'Sets your current location.', 'syntax' => "#{prefix}**(type) (location)**"}
cmdInfo['gender'] = {'description' => 'Sets the gender for the bot to refer to you as.', 'syntax' => "#{prefix}gender **(gender)**"}
cmdInfo['uinfo'] = {'description' => 'Get information on a particular user.', 'syntax' => "#{prefix}uinfo (user)"}
optionspl = {output: 'whattoplay.webm', format: '249/250/251/http_mp3_128_preview/240p/hls_opus_64/http_mp3_128/hls_mp3_128/240p'}
lastMsg = ''
bot.should_parse_self = true
embedcolour = '#A20808'


bot.message_update(){|msg|
  #pr:name
  if msg.text.gsub(' ', '').downcase.start_with? "#{prefix}name"
    if msg.text.length > 83
      msg.respond "That's too long of a name."
    elsif ((msg.message.mentions.count > 0) || (msg.text.include?'<@&')) || ((msg.text.include?'@here') || (msg.text.include? '@everyo'))
      msg.respond 'I won\'t ping anyone.'
    else
      data = YAML.load(File.open("data.yml"))
      data[msg.user.id.to_s] = (msg.text.gsub!"#{prefix}name ", '') if (msg.text.gsub"#{prefix}name", '') != nil
      data[msg.user.id.to_s] = msg.user.username if msg.text.downcase == "#{prefix}name"
      File.open("data.yml", "w") {|f| f.write(data.to_yaml)}
      msg.respond "Sure, I'll refer to you as \"#{data[msg.user.id.to_s]}\"." if msg.user.username != data[msg.user.id.to_s]
      msg.respond "Sure, I'll refer to you by your username." if msg.user.username == data[msg.user.id.to_s]
    end
  end

  #pr:gender
  if msg.text.gsub(' ', '').downcase.start_with? "#{prefix}gender"
    case msg.text.downcase.split()[1]
    when 'he/him', 'male', 'm'
      msg.respond 'Sure, I\'ll refer to you as male.'
      data = YAML.load(File.open("data.yml"))
      data[msg.user.id.to_s + 'gender'] = 'Male'
      File.open("data.yml", "w") {|f| f.write(data.to_yaml)}
    when 'she/her', 'female', 'f'
      msg.respond 'Sure, I\'ll refer to you as female.'
      data = YAML.load(File.open("data.yml"))
      data[msg.user.id.to_s + 'gender'] = 'Female'
      File.open("data.yml", "w") {|f| f.write(data.to_yaml)}
    when 'they/them', 'other', 'o'
      msg.respond 'Sure, I\'ll refer to you as other.'
      data = YAML.load(File.open("data.yml"))
      data[msg.user.id.to_s + 'gender'] = 'Other'
      File.open("data.yml", "w") {|f| f.write(data.to_yaml)}
    else
      msg.respond 'I\'ll just set your gender to other. If you\'d rather not be, please use "she/her" or "he/him."'
      data = YAML.load(File.open("data.yml"))
      data[msg.user.id.to_s + 'gender'] = 'Other'
      File.open("data.yml", "w") {|f| f.write(data.to_yaml)}
    end
  end

  #pr:location
  if msg.text.downcase.start_with? "#{prefix}location"
    case msg.text.downcase.split()[1]
    when 'city'
      msg.respond 'Coming soon.'
    when 'state', 'province'
      msg.respond 'Coming soon.'
      #data = YAML.load(File.open("data.yml"))
      #data[msg.user.id.to_s + 'location'] = loc
      #File.open("data.yml", "w") {|f| f.write(data.to_yaml)}
    when 'country'
      msg.respond 'Coming soon.'
      #data = YAML.load(File.open("data.yml"))
      #data[msg.user.id.to_s + 'location'] = loc
      #File.open("data.yml", "w") {|f| f.write(data.to_yaml)}
    when 'continent'
      case msg.text.downcase.gsub("#{prefix}location continent ", '')
      when 'na', 'north america'
        loc = 'North America'
        data = YAML.load(File.open("data.yml"))
        data[msg.user.id.to_s + 'location'] = loc
        File.open("data.yml", "w") {|f| f.write(data.to_yaml)}
        msg.respond "Okay, you will now appear to be from #{data[msg.user.id.to_s + 'location']}."
      when 'sa', 'south america'
        loc = 'South America'
        data = YAML.load(File.open("data.yml"))
        data[msg.user.id.to_s + 'location'] = loc
        File.open("data.yml", "w") {|f| f.write(data.to_yaml)}
        msg.respond "Okay, you will now appear to be from #{data[msg.user.id.to_s + 'location']}."
      when 'eu', 'europe'
        loc = 'Europe'
        data = YAML.load(File.open("data.yml"))
        data[msg.user.id.to_s + 'location'] = loc
        File.open("data.yml", "w") {|f| f.write(data.to_yaml)}
        msg.respond "Okay, you will now appear to be from #{data[msg.user.id.to_s + 'location']}."
      when 'asia'
        loc = 'Asia'
        data = YAML.load(File.open("data.yml"))
        data[msg.user.id.to_s + 'location'] = loc
        File.open("data.yml", "w") {|f| f.write(data.to_yaml)}
        msg.respond "Okay, you will now appear to be from #{data[msg.user.id.to_s + 'location']}."
      when 'oceania', 'australia'
        loc = 'Australia'
        data = YAML.load(File.open("data.yml"))
        data[msg.user.id.to_s + 'location'] = loc
        File.open("data.yml", "w") {|f| f.write(data.to_yaml)}
        msg.respond "Okay, you will now appear to be from #{data[msg.user.id.to_s + 'location']}."
      when 'antarctica'
        loc = 'Antarctica'
        data = YAML.load(File.open("data.yml"))
        data[msg.user.id.to_s + 'location'] = loc
        File.open("data.yml", "w") {|f| f.write(data.to_yaml)}
        msg.respond "Okay, you will now appear to be from #{data[msg.user.id.to_s + 'location']}."
      when 'africa'
        loc = 'Africa'
        data = YAML.load(File.open("data.yml"))
        data[msg.user.id.to_s + 'location'] = loc
        File.open("data.yml", "w") {|f| f.write(data.to_yaml)}
        msg.respond "Okay, you will now appear to be from #{data[msg.user.id.to_s + 'location']}."
      else
        msg.respond 'Please enter a valid continent.'
      end
    end
  end

  #pr:uinfo
  if msg.text.gsub(' ', '').downcase.start_with? "#{prefix}uinfo"
    if msg.message.mentions.count < 1
      msg.channel.send_embed{|embed|
        embed.title = "User Information || #{msg.user.name}##{msg.user.discrim}";
        embed.colour = embedcolour;
        embed.fields = [Discordrb::Webhooks::EmbedField.new(name: 'Acount Dates', value: "**Creation Date\:** #{Discordrb.timestamp(msg.user.creation_time, :short_datetime)} \n**Join Date\:** #{Discordrb.timestamp(msg.user.joined_at, :short_datetime)}"), Discordrb::Webhooks::EmbedField.new(name: 'Names', value: "**Username\:** #{msg.user.name} \n**Display Name\:** #{msg.user.nick || msg.user.name}"), Discordrb::Webhooks::EmbedField.new(name: 'Bot Info', value: "**Name:**   #{(YAML.load(File.open("data.yml"))[msg.user.id.to_s] || '*not set*')} \n**Gender:** #{(YAML.load(File.open("data.yml"))[msg.user.id.to_s + 'gender'] || '*not set*')} \n**Location:** #{(YAML.load(File.open("data.yml"))[msg.user.id.to_s + 'location'] || '*not set*')}")];
        embed.footer = Discordrb::Webhooks::EmbedFooter.new(text: "Precipitation.rb " + version, icon_url: "https://cdn.discordapp.com/avatars/982908116163260416/ba2793e324adb306cb45e31a41ac5f9a.png?size=64")
      }
    else
      msg.channel.send_embed{|embed|
        embed.title = "User Information || #{msg.message.mentions[0].name}##{msg.message.mentions[0].discrim}";
        embed.colour = embedcolour;
        embed.fields = [Discordrb::Webhooks::EmbedField.new(name: 'Acount Dates', value: "**Creation Date\:** #{Discordrb.timestamp(msg.message.mentions[0].on(msg.server.id).creation_time, :short_datetime)} \n**Join Date\:** #{Discordrb.timestamp(msg.message.mentions[0].on(msg.server.id).joined_at, :short_datetime)}"), Discordrb::Webhooks::EmbedField.new(name: 'Names', value: "**Username\:** #{msg.message.mentions[0].on(msg.server.id).name} \n**Display Name\:** #{msg.message.mentions[0].on(msg.server.id).nick || msg.message.mentions[0].on(msg.server.id).name}"), Discordrb::Webhooks::EmbedField.new(name: 'Bot Info', value: "**Name:**   #{(YAML.load(File.open("data.yml"))[msg.message.mentions[0].id.to_s] || '*not set*')} \n**Gender:** #{(YAML.load(File.open("data.yml"))[msg.message.mentions[0].id.to_s + 'gender'] || '*not set*')} \n**Location:** #{(YAML.load(File.open("data.yml"))[msg.message.mentions[0].id.to_s + 'location'] || '*not set*')}")];
        embed.footer = Discordrb::Webhooks::EmbedFooter.new(text: "Precipitation.rb " + version, icon_url: "https://cdn.discordapp.com/avatars/982908116163260416/ba2793e324adb306cb45e31a41ac5f9a.png?size=64")
      }
    end
  end

  #pr:play-search
    if ((msg.text.downcase.start_with? "#{prefix}play-search"))
      msg.message.reply 'Searching for music isn\'t supported yet, try using `prb:play-link` and giving a link to a song.'
    end

    #pr:play-link
    if msg.text.downcase.start_with? "#{prefix}play-link"
     if msg.voice != nil
      msg.respond 'Precipitation.rb is already playing audio to a voice channel! Use `prb:leave` to get it to disconnect, and then try playing your song again!'
     else
       if ((msg.text.include? 'soundc') ||( msg.text.include? 'pornh'))
         vb=bot.voice_connect(msg.user.voice_channel) #if ((msg.text.include? 'soundc') ||( msg.text.include? 'pornh'))
         msg.respond 'Audio will start playing shortly!' #if ((msg.text.include? 'soundc') ||( msg.text.include? 'pornh'))
       else
         msg.respond 'Playing music from the selected platform isn\'t supported, try using SoundCloud!' #nless (msg.text.include? 'soundc') ||( msg.text.include? 'pornh')
       end
      if File.file?('whattoplay.webm')
        `rm /root/folder/precipitation/whattoplay.webm`
      end
     if ((msg.text.include? 'soundc') ||( msg.text.include? 'ornhu'))
      YoutubeDL.download msg.text.split[1], optionspl
      vb.encoder.bitrate=(24000)
      vb.adjust_average = true
      vb.length_override = 7
     # vb.adjust_interval =
     # vb.adjust_offset = 20
      vb.play_file 'whattoplay.webm'
      end
      end
     end

    #pr:leave
    if msg.text.downcase == "#{prefix}leave"
      if msg.voice != nil
        msg.respond 'Disconnecting from voice channel.'
        msg.voice.destroy
      else
        msg.respond 'The bot isn\'t even in a VC, smh.'
      end
    end

  #pr:about
  if msg.text.downcase.gsub(' ', '') == "#{prefix}about"
    msg.send_embed {|embed| embed.title = 'Precipitation.rb Alpha 0.2'; embed.colour = embedcolour; embed.description = "General-purpose Discord bot"; embed.fields = [Discordrb::Webhooks::EmbedField.new(name: 'Credits', value: "**Idk837384#8148** - bot developer \n**raina#7847** - creator of the OG"), Discordrb::Webhooks::EmbedField.new(name: 'Next Release', value: 'TBD [a_0.3]'), Discordrb::Webhooks::EmbedField.new(name: 'Links', value: "[Source](https://gitlab.com/idksesolangs/precipitation.rb) \n[Support Server](https://discordapp.com/invite/UDuF9SPXxw) \n[Invite Precipitation.rb](https://discord.com/api/oauth2/authorize?client_id=1082791237662281808&permissions=8&scope=bot)")]}
    #\n**Credits \nIdk837384#8148 - bot developer \nraina#7847 - creator of the OG**
  end

  #pr:uptime
  if msg.text.downcase.gsub(' ', '') == "#{prefix}uptime"
    msg.message.reply "Precipitation.rb has been online since #{uptime}"
  end

  #pr:ping
  if msg.text.downcase.gsub(' ', '') == "#{prefix}ping"
    pingMessages = ["Pinging...", "Ponging...", "pay my onlyfans", "doin ya mom", "don't care + didn't ask", "ooh, ee, ooh ah ah, ting tang, wallawallabingbang", "omg, you're a redditor AND a discord mod?", "i use NetBSD btw", "I've come to make an announcement", "police crash green screen", "apparently i committed tax fraud but idk how that happened, i dont even pay tax????", "A Discord Bot where it can do almost nothing and no moderation stuff and made in discordrb!!!!", "did you know that 100% of people who drown die", "...\n\n\n\n\n\n\n\n whoa what is this", "Fortnite Funnies Vol. 1", "Poopenfarten"]
    random = rand(0..pingMessages.length-1)
    m = msg.respond(pingMessages[random])
    m.edit "<:ping_transmit:502755300017700865> (#{((Time.now - msg.timestamp) * 1000).round}ms) Hey, #{(YAML.load(File.open("data.yml"))[msg.user.id.to_s] || msg.user.name)}!"
  end

  #pr:version
  if msg.text.downcase.gsub(' ', '') == "#{prefix}version"
    msg.respond "Precipitation.rb #{version_name}"
  end

  #pr:help
  if msg.text.downcase.start_with? "#{prefix}help"
    if cmdInfo[msg.text.gsub("#{prefix}help ", '')] == nil
      msg.message.reply "Current Commands: \n*~uptime \n~about \n~version \n~help \n~ping \n~pic \n~play-link \n~play-search \n~name \n~location \n~gender*"
    else
      msg.channel.send_embed {|embed| embed.colour = embedcolour; embed.title = "Precipitation Index || #{prefix}#{msg.text.downcase.split()[1]}"; embed.fields =  [Discordrb::Webhooks::EmbedField.new(name: 'Description', value: cmdInfo[msg.text.downcase.split()[1]]['description']), Discordrb::Webhooks::EmbedField.new(name: 'Syntax', value: cmdInfo[msg.text.downcase.split()[1]]['syntax'])]; embed.footer = Discordrb::Webhooks::EmbedFooter.new(text: "Precipitation.rb " + version + " || bolded is a required argument, () is an argument, [] is an option", icon_url: "https://cdn.discordapp.com/avatars/982908116163260416/ba2793e324adb306cb45e31a41ac5f9a.png?size=64")}
#">>> **__Precipitation Index || prb;#{msg.text.downcase.split()[1]}__** \n\n**Description** \n#{cmdInfo[msg.text.downcase.split()[1]]['description']} \n**Syntax** \n#{cmdInfo[msg.text.downcase.split()[1]]['syntax']}"
    end
  end

  #pr:pic
  if msg.text.downcase.split[0] == "#{prefix}pic"
    if msg.message.mentions[0] != nil
      msg.channel.send_embed {|embed| embed.colour = embedcolour; embed.title = (msg.message.mentions[0].name || msg.user.name) + "#" + (msg.message.mentions[0].discrim || msg.user.discrim) + "'s Profile Picture"; embed.footer = Discordrb::Webhooks::EmbedFooter.new(text: "Precipitation.rb " + version, icon_url: "https://cdn.discordapp.com/avatars/982908116163260416/ba2793e324adb306cb45e31a41ac5f9a.png?size=64"); embed.image = Discordrb::Webhooks::EmbedImage.new(url: (msg.message.mentions[0] || msg.user).avatar_url('jpg'));}
    else
      msg.channel.send_embed {|embed| embed.colour = embedcolour; embed.title = (msg.user.name) + "#" + (msg.user.discrim) + "'s Profile Picture"; embed.footer = Discordrb::Webhooks::EmbedFooter.new(text: "Precipitation.rb " + version, icon_url: "https://cdn.discordapp.com/avatars/982908116163260416/ba2793e324adb306cb45e31a41ac5f9a.png?size=64"); embed.image = Discordrb::Webhooks::EmbedImage.new(url: (msg.user).avatar_url('jpg'));}
    end
  end
}
bot.message(){|msg|
  #pr:name
  if msg.text.gsub(' ', '').downcase.start_with? "#{prefix}name"
    if msg.text.length > 83
      msg.respond "That's too long of a name."
    elsif ((msg.message.mentions.count > 0) || (msg.text.include?'<@&')) || ((msg.text.include?'@here') || (msg.text.include? '@everyo'))
      msg.respond 'I won\'t ping anyone.'
    else
      data = YAML.load(File.open("data.yml"))
      data[msg.user.id.to_s] = (msg.text.gsub!"#{prefix}name ", '') if (msg.text.gsub"#{prefix}name", '') != nil
      data[msg.user.id.to_s] = msg.user.username if msg.text.downcase == "#{prefix}name"
      File.open("data.yml", "w") {|f| f.write(data.to_yaml)}
      msg.respond "Sure, I'll refer to you as \"#{data[msg.user.id.to_s]}\"." if msg.user.username != data[msg.user.id.to_s]
      msg.respond "Sure, I'll refer to you by your username." if msg.user.username == data[msg.user.id.to_s]
    end
  end

  #pr:gender
  if msg.text.gsub(' ', '').downcase.start_with? "#{prefix}gender"
    case msg.text.downcase.split()[1]
    when 'he/him', 'male', 'm'
      msg.respond 'Sure, I\'ll refer to you as male.'
      data = YAML.load(File.open("data.yml"))
      data[msg.user.id.to_s + 'gender'] = 'Male'
      File.open("data.yml", "w") {|f| f.write(data.to_yaml)}
    when 'she/her', 'female', 'f'
      msg.respond 'Sure, I\'ll refer to you as female.'
      data = YAML.load(File.open("data.yml"))
      data[msg.user.id.to_s + 'gender'] = 'Female'
      File.open("data.yml", "w") {|f| f.write(data.to_yaml)}
    when 'they/them', 'other', 'o'
      msg.respond 'Sure, I\'ll refer to you as other.'
      data = YAML.load(File.open("data.yml"))
      data[msg.user.id.to_s + 'gender'] = 'Other'
      File.open("data.yml", "w") {|f| f.write(data.to_yaml)}
    else
      msg.respond 'I\'ll just set your gender to other. If you\'d rather not be, please use "she/her" or "he/him."'
      data = YAML.load(File.open("data.yml"))
      data[msg.user.id.to_s + 'gender'] = 'Other'
      File.open("data.yml", "w") {|f| f.write(data.to_yaml)}
    end
  end

  #pr:location
  if msg.text.downcase.start_with? "#{prefix}location"
    case msg.text.downcase.split()[1]
    when 'city'
      msg.respond 'Coming soon.'
    when 'state', 'province'
      msg.respond 'Coming soon.'
      #data = YAML.load(File.open("data.yml"))
      #data[msg.user.id.to_s + 'location'] = loc
      #File.open("data.yml", "w") {|f| f.write(data.to_yaml)}
    when 'country'
      msg.respond 'Coming soon.'
      #data = YAML.load(File.open("data.yml"))
      #data[msg.user.id.to_s + 'location'] = loc
      #File.open("data.yml", "w") {|f| f.write(data.to_yaml)}
    when 'continent'
      case msg.text.downcase.gsub("#{prefix}location continent ", '')
      when 'na', 'north america'
        loc = 'North America'
        data = YAML.load(File.open("data.yml"))
        data[msg.user.id.to_s + 'location'] = loc
        File.open("data.yml", "w") {|f| f.write(data.to_yaml)}
        msg.respond "Okay, you will now appear to be from #{data[msg.user.id.to_s + 'location']}."
      when 'sa', 'south america'
        loc = 'South America'
        data = YAML.load(File.open("data.yml"))
        data[msg.user.id.to_s + 'location'] = loc
        File.open("data.yml", "w") {|f| f.write(data.to_yaml)}
        msg.respond "Okay, you will now appear to be from #{data[msg.user.id.to_s + 'location']}."
      when 'eu', 'europe'
        loc = 'Europe'
        data = YAML.load(File.open("data.yml"))
        data[msg.user.id.to_s + 'location'] = loc
        File.open("data.yml", "w") {|f| f.write(data.to_yaml)}
        msg.respond "Okay, you will now appear to be from #{data[msg.user.id.to_s + 'location']}."
      when 'asia'
        loc = 'Asia'
        data = YAML.load(File.open("data.yml"))
        data[msg.user.id.to_s + 'location'] = loc
        File.open("data.yml", "w") {|f| f.write(data.to_yaml)}
        msg.respond "Okay, you will now appear to be from #{data[msg.user.id.to_s + 'location']}."
      when 'oceania', 'australia'
        loc = 'Australia'
        data = YAML.load(File.open("data.yml"))
        data[msg.user.id.to_s + 'location'] = loc
        File.open("data.yml", "w") {|f| f.write(data.to_yaml)}
        msg.respond "Okay, you will now appear to be from #{data[msg.user.id.to_s + 'location']}."
      when 'antarctica'
        loc = 'Antarctica'
        data = YAML.load(File.open("data.yml"))
        data[msg.user.id.to_s + 'location'] = loc
        File.open("data.yml", "w") {|f| f.write(data.to_yaml)}
        msg.respond "Okay, you will now appear to be from #{data[msg.user.id.to_s + 'location']}."
      when 'africa'
        loc = 'Africa'
        data = YAML.load(File.open("data.yml"))
        data[msg.user.id.to_s + 'location'] = loc
        File.open("data.yml", "w") {|f| f.write(data.to_yaml)}
        msg.respond "Okay, you will now appear to be from #{data[msg.user.id.to_s + 'location']}."
      else
        msg.respond 'Please enter a valid continent.'
      end
    end
  end

  #pr:uinfo
  if msg.text.gsub(' ', '').downcase.start_with? "#{prefix}uinfo"
    if msg.message.mentions.count < 1
      msg.channel.send_embed{|embed|
        embed.title = "User Information || #{msg.user.name}##{msg.user.discrim}";
        embed.colour = embedcolour;
        embed.fields = [Discordrb::Webhooks::EmbedField.new(name: 'Acount Dates', value: "**Creation Date\:** #{Discordrb.timestamp(msg.user.creation_time, :short_datetime)} \n**Join Date\:** #{Discordrb.timestamp(msg.user.joined_at, :short_datetime)}"), Discordrb::Webhooks::EmbedField.new(name: 'Names', value: "**Username\:** #{msg.user.name} \n**Display Name\:** #{msg.user.nick || msg.user.name}"), Discordrb::Webhooks::EmbedField.new(name: 'Bot Info', value: "**Name:**   #{(YAML.load(File.open("data.yml"))[msg.user.id.to_s] || '*not set*')} \n**Gender:** #{(YAML.load(File.open("data.yml"))[msg.user.id.to_s + 'gender'] || '*not set*')} \n**Location:** #{(YAML.load(File.open("data.yml"))[msg.user.id.to_s + 'location'] || '*not set*')}")];
        embed.footer = Discordrb::Webhooks::EmbedFooter.new(text: "Precipitation.rb " + version, icon_url: "https://cdn.discordapp.com/avatars/982908116163260416/ba2793e324adb306cb45e31a41ac5f9a.png?size=64")
      }
    else
      msg.channel.send_embed{|embed|
        embed.title = "User Information || #{msg.message.mentions[0].name}##{msg.message.mentions[0].discrim}";
        embed.colour = embedcolour;
        embed.fields = [Discordrb::Webhooks::EmbedField.new(name: 'Acount Dates', value: "**Creation Date\:** #{Discordrb.timestamp(msg.message.mentions[0].on(msg.server.id).creation_time, :short_datetime)} \n**Join Date\:** #{Discordrb.timestamp(msg.message.mentions[0].on(msg.server.id).joined_at, :short_datetime)}"), Discordrb::Webhooks::EmbedField.new(name: 'Names', value: "**Username\:** #{msg.message.mentions[0].on(msg.server.id).name} \n**Display Name\:** #{msg.message.mentions[0].on(msg.server.id).nick || msg.message.mentions[0].on(msg.server.id).name}"), Discordrb::Webhooks::EmbedField.new(name: 'Bot Info', value: "**Name:**   #{(YAML.load(File.open("data.yml"))[msg.message.mentions[0].id.to_s] || '*not set*')} \n**Gender:** #{(YAML.load(File.open("data.yml"))[msg.message.mentions[0].id.to_s + 'gender'] || '*not set*')} \n**Location:** #{(YAML.load(File.open("data.yml"))[msg.message.mentions[0].id.to_s + 'location'] || '*not set*')}")];
        embed.footer = Discordrb::Webhooks::EmbedFooter.new(text: "Precipitation.rb " + version, icon_url: "https://cdn.discordapp.com/avatars/982908116163260416/ba2793e324adb306cb45e31a41ac5f9a.png?size=64")
      }
    end
  end

  #pr:play-search
    if ((msg.text.downcase.start_with? "#{prefix}play-search"))
      msg.message.reply 'Searching for music isn\'t supported yet, try using `prb:play-link` and giving a link to a song.'
    end

    #pr:play-link
    if msg.text.downcase.start_with? "#{prefix}play-link"
     if msg.voice != nil
      msg.respond 'Precipitation.rb is already playing audio to a voice channel! Use `prb:leave` to get it to disconnect, and then try playing your song again!'
     else
       if ((msg.text.include? 'soundc') ||( msg.text.include? 'pornh'))
         vb=bot.voice_connect(msg.user.voice_channel) #if ((msg.text.include? 'soundc') ||( msg.text.include? 'pornh'))
         msg.respond 'Audio will start playing shortly!' #if ((msg.text.include? 'soundc') ||( msg.text.include? 'pornh'))
       else
         msg.respond 'Playing music from the selected platform isn\'t supported, try using SoundCloud!' #nless (msg.text.include? 'soundc') ||( msg.text.include? 'pornh')
       end
      if File.file?('whattoplay.webm')
        `rm /root/folder/precipitation/whattoplay.webm`
      end
     if ((msg.text.include? 'soundc') ||( msg.text.include? 'ornhu'))
      YoutubeDL.download msg.text.split[1], optionspl
      vb.encoder.bitrate=(24000)
      vb.adjust_average = true
      vb.length_override = 7
     # vb.adjust_interval =
     # vb.adjust_offset = 20
      vb.play_file 'whattoplay.webm'
      end
      end
     end

    #pr:leave
    if msg.text.downcase == "#{prefix}leave"
      if msg.voice != nil
        msg.respond 'Disconnecting from voice channel.'
        msg.voice.destroy
      else
        msg.respond 'The bot isn\'t even in a VC, smh.'
      end
    end

  #pr:about
  if msg.text.downcase.gsub(' ', '') == "#{prefix}about"
    msg.send_embed {|embed| embed.title = 'Precipitation.rb Alpha 0.2'; embed.colour = embedcolour; embed.description = "General-purpose Discord bot"; embed.fields = [Discordrb::Webhooks::EmbedField.new(name: 'Credits', value: "**Idk837384#8148** - bot developer \n**raina#7847** - creator of the OG"), Discordrb::Webhooks::EmbedField.new(name: 'Next Release', value: 'TBD [a_0.3]'), Discordrb::Webhooks::EmbedField.new(name: 'Links', value: "[Source](https://gitlab.com/idksesolangs/precipitation.rb) \n[Support Server](https://discordapp.com/invite/UDuF9SPXxw) \n[Invite Precipitation.rb](https://discord.com/api/oauth2/authorize?client_id=1082791237662281808&permissions=8&scope=bot)")]}
    #\n**Credits \nIdk837384#8148 - bot developer \nraina#7847 - creator of the OG**
  end

  #pr:uptime
  if msg.text.downcase.gsub(' ', '') == "#{prefix}uptime"
    msg.message.reply "Precipitation.rb has been online since #{uptime}"
  end

  #pr:ping
  if msg.text.downcase.gsub(' ', '') == "#{prefix}ping"
    pingMessages = ["Pinging...", "Ponging...", "pay my onlyfans", "doin ya mom", "don't care + didn't ask", "ooh, ee, ooh ah ah, ting tang, wallawallabingbang", "omg, you're a redditor AND a discord mod?", "i use NetBSD btw", "I've come to make an announcement", "police crash green screen", "apparently i committed tax fraud but idk how that happened, i dont even pay tax????", "A Discord Bot where it can do almost nothing and no moderation stuff and made in discordrb!!!!", "did you know that 100% of people who drown die", "...\n\n\n\n\n\n\n\n whoa what is this", "Fortnite Funnies Vol. 1", "Poopenfarten"]
    random = rand(0..pingMessages.length-1)
    m = msg.respond(pingMessages[random])
    m.edit "<:ping_transmit:502755300017700865> (#{((Time.now - msg.timestamp) * 1000).round}ms) Hey, #{(YAML.load(File.open("data.yml"))[msg.user.id.to_s] || msg.user.name)}!"
  end


  #pr:version
  if msg.text.downcase.gsub(' ', '') == "#{prefix}version"
    msg.respond "Precipitation.rb #{version_name}"
  end

  #pr:help
  if msg.text.downcase.start_with? "#{prefix}help"
    if cmdInfo[msg.text.gsub("#{prefix}help ", '')] == nil
      msg.message.reply "Current Commands: \n*~uptime \n~about \n~version \n~help \n~ping \n~pic \n~play-link \n~play-search \n~name \n~location \n~gender*"
    else
      msg.channel.send_embed {|embed| embed.colour = embedcolour; embed.title = "Precipitation Index || #{prefix}#{msg.text.downcase.split()[1]}"; embed.fields =  [Discordrb::Webhooks::EmbedField.new(name: 'Description', value: cmdInfo[msg.text.downcase.split()[1]]['description']), Discordrb::Webhooks::EmbedField.new(name: 'Syntax', value: cmdInfo[msg.text.downcase.split()[1]]['syntax'])]; embed.footer = Discordrb::Webhooks::EmbedFooter.new(text: "Precipitation.rb " + version + " || bolded is a required argument, () is an argument, [] is an option", icon_url: "https://cdn.discordapp.com/avatars/982908116163260416/ba2793e324adb306cb45e31a41ac5f9a.png?size=64")}
#">>> **__Precipitation Index || prb;#{msg.text.downcase.split()[1]}__** \n\n**Description** \n#{cmdInfo[msg.text.downcase.split()[1]]['description']} \n**Syntax** \n#{cmdInfo[msg.text.downcase.split()[1]]['syntax']}"
    end
  end

  #pr:pic
  if msg.text.downcase.split[0] == "#{prefix}pic"
    if msg.message.mentions[0] != nil
      msg.channel.send_embed {|embed| embed.colour = embedcolour; embed.title = (msg.message.mentions[0].name || msg.user.name) + "#" + (msg.message.mentions[0].discrim || msg.user.discrim) + "'s Profile Picture"; embed.footer = Discordrb::Webhooks::EmbedFooter.new(text: "Precipitation.rb " + version, icon_url: "https://cdn.discordapp.com/avatars/982908116163260416/ba2793e324adb306cb45e31a41ac5f9a.png?size=64"); embed.image = Discordrb::Webhooks::EmbedImage.new(url: (msg.message.mentions[0] || msg.user).avatar_url('jpg'));}
    else
      msg.channel.send_embed {|embed| embed.colour = embedcolour; embed.title = (msg.user.name) + "#" + (msg.user.discrim) + "'s Profile Picture"; embed.footer = Discordrb::Webhooks::EmbedFooter.new(text: "Precipitation.rb " + version, icon_url: "https://cdn.discordapp.com/avatars/982908116163260416/ba2793e324adb306cb45e31a41ac5f9a.png?size=64"); embed.image = Discordrb::Webhooks::EmbedImage.new(url: (msg.user).avatar_url('jpg'));}
    end
  end
}




=begin
#testing:
require 'yaml'
testbot = bot
testuptime = Discordrb.timestamp(Time.now, :short_datetime)
testscheduler = Rufus::Scheduler.new
testcurrentGames = {}
testversion = 'a_0.2-testing'
testversion_name = testversion + ' Jasper'
testcmdInfo = {}

testbot.message_update(){|msg|
  #pr:name
  if msg.text.gsub(' ', '').downcase.start_with? 'prb;name'
    testcmdInfo['name'] = {'description' => 'Sets the name for the bot to refer to you as.', 'syntax' => 'pr;name (name)'}
    if msg.text.length > 83
      msg.respond "That's too long of a name."
    elsif ((msg.message.mentions.count > 0) || (msg.text.include?'<@&')) || ((msg.text.include?'@here') || (msg.text.include? '@everyo'))
      msg.respond 'I won\'t ping anyone.'
    else
      data = YAML.load(File.open("data.yml"))
      data[msg.user.id.to_s] = (msg.text.gsub!"prb;name ", '') if (msg.text.gsub"prb;name", '') != nil
      data[msg.user.id.to_s] = msg.user.username if msg.text.downcase == 'prb;name'
      File.open("data.yml", "w") {|f| f.write(data.to_yaml)}
      msg.respond "Sure, I'll refer to you as \"#{data[msg.user.id.to_s]}\"." if msg.user.username != data[msg.user.id.to_s]
      msg.respond "Sure, I'll refer to you by your username." if msg.user.username == data[msg.user.id.to_s]
    end
  end

  #pr:gender
  if msg.text.gsub(' ', '').downcase.start_with? 'prb;gender'
    case msg.text.downcase.split()[1]
    when 'he/him', 'male'
      msg.respond 'Sure, I\'ll refer to you as male.'
      data = YAML.load(File.open("data.yml"))
      data[msg.user.id.to_s + 'gender'] = 'm'
      File.open("data.yml", "w") {|f| f.write(data.to_yaml)}
    when 'she/her', 'female'
      msg.respond 'Sure, I\'ll refer to you as female.'
      data = YAML.load(File.open("data.yml"))
      data[msg.user.id.to_s + 'gender'] = 'f'
      File.open("data.yml", "w") {|f| f.write(data.to_yaml)}
    when 'they/them', 'other'
      msg.respond 'Sure, I\'ll refer to you as other.'
      data = YAML.load(File.open("data.yml"))
      data[msg.user.id.to_s + 'gender'] = 'o'
      File.open("data.yml", "w") {|f| f.write(data.to_yaml)}
    else
      msg.respond 'Please run the command again, but with the gender argument.'

    end
  end

  #pr:location
  if msg.text.downcase.start_with? 'prb;location'
    case msg.text.downcase.split()[1]
    when 'city'
      msg.respond 'Coming soon.'
    when 'state', 'province'
      msg.respond 'Coming soon.'
      #data = YAML.load(File.open("data.yml"))
      #data[msg.user.id.to_s + 'location'] = loc
      #File.open("data.yml", "w") {|f| f.write(data.to_yaml)}
    when 'country'
      msg.respond 'Coming soon.'
      #data = YAML.load(File.open("data.yml"))
      #data[msg.user.id.to_s + 'location'] = loc
      #File.open("data.yml", "w") {|f| f.write(data.to_yaml)}
    when 'continent'
      case msg.text.downcase.gsub('prb;location continent ', '')
      when 'na', 'north america'
        loc = 'North America'
        data = YAML.load(File.open("data.yml"))
        data[msg.user.id.to_s + 'location'] = loc
        File.open("data.yml", "w") {|f| f.write(data.to_yaml)}
        msg.respond "Okay, you will now appear to be from #{data[msg.user.id.to_s + 'location']}."
      when 'sa', 'south america'
        loc = 'South America'
        data = YAML.load(File.open("data.yml"))
        data[msg.user.id.to_s + 'location'] = loc
        File.open("data.yml", "w") {|f| f.write(data.to_yaml)}
        msg.respond "Okay, you will now appear to be from #{data[msg.user.id.to_s + 'location']}."
      when 'eu', 'europe'
        loc = 'Europe'
        data = YAML.load(File.open("data.yml"))
        data[msg.user.id.to_s + 'location'] = loc
        File.open("data.yml", "w") {|f| f.write(data.to_yaml)}
        msg.respond "Okay, you will now appear to be from #{data[msg.user.id.to_s + 'location']}."
      when 'asia'
        loc = 'Asia'
        data = YAML.load(File.open("data.yml"))
        data[msg.user.id.to_s + 'location'] = loc
        File.open("data.yml", "w") {|f| f.write(data.to_yaml)}
        msg.respond "Okay, you will now appear to be from #{data[msg.user.id.to_s + 'location']}."
      when 'oceania', 'australia'
        loc = 'Australia'
        data = YAML.load(File.open("data.yml"))
        data[msg.user.id.to_s + 'location'] = loc
        File.open("data.yml", "w") {|f| f.write(data.to_yaml)}
        msg.respond "Okay, you will now appear to be from #{data[msg.user.id.to_s + 'location']}."
      when 'antarctica'
        loc = 'Antarctica'
        data = YAML.load(File.open("data.yml"))
        data[msg.user.id.to_s + 'location'] = loc
        File.open("data.yml", "w") {|f| f.write(data.to_yaml)}
        msg.respond "Okay, you will now appear to be from #{data[msg.user.id.to_s + 'location']}."
      when 'africa'
        loc = 'Africa'
        data = YAML.load(File.open("data.yml"))
        data[msg.user.id.to_s + 'location'] = loc
        File.open("data.yml", "w") {|f| f.write(data.to_yaml)}
        msg.respond "Okay, you will now appear to be from #{data[msg.user.id.to_s + 'location']}."
      else
        msg.respond 'Please enter a valid continent.'
      end
    end
  end

  #pr:play-search
  if ((msg.text.downcase.gsub(' ', '').start_with? 'prb;play-search'))
    msg.respond 'Audio commands are not supported on Precipitation.rb/Testing!'
  end

  #pr:play-link
  if msg.text.downcase.gsub(' ', '').start_with? 'prb;play-link'
    msg.respond 'Audio commands are not supported on Precipitation.rb/Testing!'
  end

  #pr:leave
  if msg.text.downcase.gsub(' ', '') == 'prb;leave'
    msg.respond 'Audio commands are not supported on Precipitation.rb/Testing!'
  end
  #pr:about
  if msg.text.downcase.gsub(' ', '') == 'prb;about'
    testcmdInfo['about'] = {'description' => 'Gives information on the bot, as well as credits.', 'syntax' => 'prb;about'}
    msg.send_embed {|embed| embed.title = 'Precipitation.rb Alpha 0.2-testing'; embed.colour = embedcolour; embed.description = "General-purpose Discord bot"; embed.fields = [Discordrb::Webhooks::EmbedField.new(name: 'Credits', value: "**Idk837384#8148** - bot developer \n**raina#7847** - creator of the OG"), Discordrb::Webhooks::EmbedField.new(name: 'Next Release', value: 'TBD [a_0.2]'), Discordrb::Webhooks::EmbedField.new(name: 'Links', value: "[Source](https://gitlab.com/idksesolangs/precipitation.rb) \n[Support Server](https://discordapp.com/invite/UDuF9SPXxw) \n[Invite Precipitation.rb](https://discord.com/api/oauth2/authorize?client_id=1082791237662281808&permissions=8&scope=bot)")]}
    #\n**Credits \nIdk837384#8148 - bot developer \nraina#7847 - creator of the OG**
  end

  #pr:uptime
  if msg.text.downcase.gsub(' ', '') == 'prb;uptime'
    testcmdInfo['uptime'] = {'description' => 'See how long Precipitation.rb has been online.', 'syntax' => 'prb;uptime'}
    msg.message.reply "Precipitation.rb has been online since #{testuptime}"
  end

  #pr:ping
  if msg.text.downcase.gsub(' ', '') == 'prb;ping'
    testcmdInfo['ping'] = {'description' => 'Gets the current latency of the bot.', 'syntax' => 'prb;ping'}
    pingMessages = ["Pinging...", "Ponging...", "pay my onlyfans", "doin ya mom", "don't care + didn't ask", "ooh, ee, ooh ah ah, ting tang, wallawallabingbang", "omg, you're a redditor AND a discord mod?", "i use NetBSD btw", "I've come to make an announcement", "police crash green screen", "apparently i committed tax fraud but idk how that happened, i dont even pay tax????", "A Discord Bot where it can do almost nothing and no moderation stuff and made in discordrb!!!!", "did you know that 100% of people who drown die", "...\n\n\n\n\n\n\n\n whoa what is this", "Fortnite Funnies Vol. 1", "Poopenfarten"]
    random = rand(0..pingMessages.length-1)
    m = msg.respond(pingMessages[random])
    m.edit "<:ping_transmit:502755300017700865> (#{((Time.now - msg.timestamp) * 1000).round}ms) Hey, #{msg.user.name}!"
  end

  #pr:version
  if msg.text.downcase.gsub(' ', '') == 'prb;version'
    testcmdInfo['version'] = {'description' => 'Shows the current bot version.', 'syntax' => 'prb;version'}
    msg.respond "Precipitation.rb #{testversion_name}"
  end

  #pr:help
  if msg.text.downcase.start_with? 'prb;help'
    testcmdInfo['help'] = {'description' => 'Gets a list of commands, or shows information about a command.', 'syntax' => 'prb;help (command)'}
    if msg.text.downcase.split()[1] == nil
      msg.message.reply "Current Commands: \n*~uptime \n~about \n~version \n~help \n~ping \n~pic \n~play-link \n~play-search*"
    else
      puts testcmdInfo
      puts testcmdInfo[msg.text.downcase.split()[1]]
      msg.channel.send_embed {|embed| embed.colour = embedcolour; embed.title = "Precipitation Index || prb;#{msg.text.downcase.split()[1]}"; embed.fields =  [Discordrb::Webhooks::EmbedField.new(name: 'Description', value: testcmdInfo[msg.text.downcase.split()[1]]['description']), Discordrb::Webhooks::EmbedField.new(name: 'Syntax', value: testcmdInfo[msg.text.downcase.split()[1]]['syntax'])]; embed.footer = Discordrb::Webhooks::EmbedFooter.new(text: "Precipitation.rb " + version + " || bolded is a required argument, () is an argument, [] is an option", icon_url: "https://cdn.discordapp.com/avatars/982908116163260416/ba2793e324adb306cb45e31a41ac5f9a.png?size=64")}
#">>> **__Precipitation Index || prb;#{msg.text.downcase.split()[1]}__** \n\n**Description** \n#{cmdInfo[msg.text.downcase.split()[1]]['description']} \n**Syntax** \n#{cmdInfo[msg.text.downcase.split()[1]]['syntax']}"
    end
  end

  #pr:pic
  if msg.text.downcase.split[0] == 'prb;pic'
    testcmdInfo['pic'] = {'description' => 'Gets the profile picture of yourself or another user.', 'syntax' => 'prb;pic (user)'}
    if msg.message.mentions[0] != nil
      msg.channel.send_embed {|embed| embed.colour = embedcolour; embed.title = (msg.message.mentions[0].name || msg.user.name) + "#" + (msg.message.mentions[0].discrim || msg.user.discrim) + "'s Profile Picture"; embed.footer = Discordrb::Webhooks::EmbedFooter.new(text: "Precipitation.rb " + version, icon_url: "https://cdn.discordapp.com/avatars/982908116163260416/ba2793e324adb306cb45e31a41ac5f9a.png?size=64"); embed.image = Discordrb::Webhooks::EmbedImage.new(url: (msg.message.mentions[0] || msg.user).avatar_url('jpg'));}
    else
      msg.channel.send_embed {|embed| embed.colour = embedcolour; embed.title = (msg.user.name) + "#" + (msg.user.discrim) + "'s Profile Picture"; embed.footer = Discordrb::Webhooks::EmbedFooter.new(text: "Precipitation.rb " + version, icon_url: "https://cdn.discordapp.com/avatars/982908116163260416/ba2793e324adb306cb45e31a41ac5f9a.png?size=64"); embed.image = Discordrb::Webhooks::EmbedImage.new(url: (msg.user).avatar_url('jpg'));}
    end
  end
}
testbot.message(){|msg|
  #pr:name
  if msg.text.gsub(' ', '').downcase.start_with? 'prb;name'
    testcmdInfo['name'] = {'description' => 'Sets the name for the bot to refer to you as.', 'syntax' => 'pr;name (name)'}
    if msg.text.length > 83
      msg.respond "That's too long of a name."
    elsif ((msg.message.mentions.count > 0) || (msg.text.include?'<@&')) || ((msg.text.include?'@here') || (msg.text.include? '@everyo'))
      msg.respond 'I won\'t ping anyone.'
    else
      data = YAML.load(File.open("data.yml"))
      data[msg.user.id.to_s] = (msg.text.gsub!"prb;name ", '') if (msg.text.gsub"prb;name", '') != nil
      data[msg.user.id.to_s] = msg.user.username if msg.text.downcase == 'prb;name'
      File.open("data.yml", "w") {|f| f.write(data.to_yaml)}
      msg.respond "Sure, I'll refer to you as \"#{data[msg.user.id.to_s]}\"." if msg.user.username != data[msg.user.id.to_s]
      msg.respond "Sure, I'll refer to you by your username." if msg.user.username == data[msg.user.id.to_s]
    end
  end

  #pr:gender
  if msg.text.gsub(' ', '').downcase.start_with? 'prb;gender'
    testcmdInfo['gender'] = {'description' => 'Sets the gender for the bot to refer to you as.', 'syntax' => 'pr;gender (gender)'}
    case msg.text.downcase.split()[1]
    when 'he/him', 'male'
      msg.respond 'Sure, I\'ll refer to you as male.'
      data = YAML.load(File.open("data.yml"))
      data[msg.user.id.to_s + 'gender'] = 'm'
      File.open("data.yml", "w") {|f| f.write(data.to_yaml)}
    when 'she/her', 'female'
      msg.respond 'Sure, I\'ll refer to you as female.'
      data = YAML.load(File.open("data.yml"))
      data[msg.user.id.to_s + 'gender'] = 'f'
      File.open("data.yml", "w") {|f| f.write(data.to_yaml)}
    when 'they/them', 'other'
      msg.respond 'Sure, I\'ll refer to you as other.'
      data = YAML.load(File.open("data.yml"))
      data[msg.user.id.to_s + 'gender'] = 'o'
      File.open("data.yml", "w") {|f| f.write(data.to_yaml)}
    else
      msg.respond 'Please run the command again, but with the gender argument.'

    end
  end

  #pr:location
  if msg.text.downcase.start_with? 'prb;location'
    case msg.text.downcase.split()[1]
    when 'city'
      msg.respond 'Coming soon.'
    when 'state', 'province'
      msg.respond 'Coming soon.'
      #data = YAML.load(File.open("data.yml"))
      #data[msg.user.id.to_s + 'location'] = loc
      #File.open("data.yml", "w") {|f| f.write(data.to_yaml)}
    when 'country'
      msg.respond 'Coming soon.'
      #data = YAML.load(File.open("data.yml"))
      #data[msg.user.id.to_s + 'location'] = loc
      #File.open("data.yml", "w") {|f| f.write(data.to_yaml)}
    when 'continent'
      case msg.text.downcase.gsub('prb;location continent ', '')
      when 'na', 'north america'
        loc = 'North America'
        data = YAML.load(File.open("data.yml"))
        data[msg.user.id.to_s + 'location'] = loc
        File.open("data.yml", "w") {|f| f.write(data.to_yaml)}
        msg.respond "Okay, you will now appear to be from #{data[msg.user.id.to_s + 'location']}."
      when 'sa', 'south america'
        loc = 'South America'
        data = YAML.load(File.open("data.yml"))
        data[msg.user.id.to_s + 'location'] = loc
        File.open("data.yml", "w") {|f| f.write(data.to_yaml)}
        msg.respond "Okay, you will now appear to be from #{data[msg.user.id.to_s + 'location']}."
      when 'eu', 'europe'
        loc = 'Europe'
        data = YAML.load(File.open("data.yml"))
        data[msg.user.id.to_s + 'location'] = loc
        File.open("data.yml", "w") {|f| f.write(data.to_yaml)}
        msg.respond "Okay, you will now appear to be from #{data[msg.user.id.to_s + 'location']}."
      when 'asia'
        loc = 'Asia'
        data = YAML.load(File.open("data.yml"))
        data[msg.user.id.to_s + 'location'] = loc
        File.open("data.yml", "w") {|f| f.write(data.to_yaml)}
        msg.respond "Okay, you will now appear to be from #{data[msg.user.id.to_s + 'location']}."
      when 'oceania', 'australia'
        loc = 'Australia'
        data = YAML.load(File.open("data.yml"))
        data[msg.user.id.to_s + 'location'] = loc
        File.open("data.yml", "w") {|f| f.write(data.to_yaml)}
        msg.respond "Okay, you will now appear to be from #{data[msg.user.id.to_s + 'location']}."
      when 'antarctica'
        loc = 'Antarctica'
        data = YAML.load(File.open("data.yml"))
        data[msg.user.id.to_s + 'location'] = loc
        File.open("data.yml", "w") {|f| f.write(data.to_yaml)}
        msg.respond "Okay, you will now appear to be from #{data[msg.user.id.to_s + 'location']}."
      when 'africa'
        loc = 'Africa'
        data = YAML.load(File.open("data.yml"))
        data[msg.user.id.to_s + 'location'] = loc
        File.open("data.yml", "w") {|f| f.write(data.to_yaml)}
        msg.respond "Okay, you will now appear to be from #{data[msg.user.id.to_s + 'location']}."
      else
        msg.respond 'Please enter a valid continent.'
      end
    end
  end


  #pr:play-search
  if ((msg.text.downcase.gsub(' ', '').start_with? 'prb;play-search'))
    msg.respond 'Audio commands are not supported on Precipitation.rb/Testing!'
  end

  #pr:play-link
  if msg.text.downcase.gsub(' ', '').start_with? 'prb;play-link'
    msg.respond 'Audio commands are not supported on Precipitation.rb/Testing!'
  end

  #pr:leave
  if msg.text.downcase.gsub(' ', '') == 'prb;leave'
    msg.respond 'Audio commands are not supported on Precipitation.rb/Testing!'
  end
  #pr:about
  if msg.text.downcase.gsub(' ', '') == 'prb;about'
    testcmdInfo['about'] = {'description' => 'Gives information on the bot, as well as credits.', 'syntax' => 'prb;about'}
    msg.send_embed {|embed| embed.title = 'Precipitation.rb Alpha 0.2-testing'; embed.colour = embedcolour; embed.description = "General-purpose Discord bot"; embed.fields = [Discordrb::Webhooks::EmbedField.new(name: 'Credits', value: "**Idk837384#8148** - bot developer \n**raina#7847** - creator of the OG"), Discordrb::Webhooks::EmbedField.new(name: 'Next Release', value: 'TBD [a_0.2]'), Discordrb::Webhooks::EmbedField.new(name: 'Links', value: "[Source](https://gitlab.com/idksesolangs/precipitation.rb) \n[Support Server](https://discordapp.com/invite/UDuF9SPXxw) \n[Invite Precipitation.rb](https://discord.com/api/oauth2/authorize?client_id=1082791237662281808&permissions=8&scope=bot)")]}
    #\n**Credits \nIdk837384#8148 - bot developer \nraina#7847 - creator of the OG**
  end

  #pr:uptime
  if msg.text.downcase.gsub(' ', '') == 'prb;uptime'
    testcmdInfo['uptime'] = {'description' => 'See how long Precipitation.rb has been online.', 'syntax' => 'prb;uptime'}
    msg.message.reply "Precipitation.rb has been online since #{testuptime}"
  end

  #pr:ping
  if msg.text.downcase.gsub(' ', '') == 'prb;ping'
    testcmdInfo['ping'] = {'description' => 'Gets the current latency of the bot.', 'syntax' => 'prb;ping'}
    pingMessages = ["Pinging...", "Ponging...", "pay my onlyfans", "doin ya mom", "don't care + didn't ask", "ooh, ee, ooh ah ah, ting tang, wallawallabingbang", "omg, you're a redditor AND a discord mod?", "i use NetBSD btw", "I've come to make an announcement", "police crash green screen", "apparently i committed tax fraud but idk how that happened, i dont even pay tax????", "A Discord Bot where it can do almost nothing and no moderation stuff and made in discordrb!!!!", "did you know that 100% of people who drown die", "...\n\n\n\n\n\n\n\n whoa what is this", "Fortnite Funnies Vol. 1", "Poopenfarten"]
    random = rand(0..pingMessages.length-1)
    m = msg.respond(pingMessages[random])
    m.edit "<:ping_transmit:502755300017700865> (#{((Time.now - msg.timestamp) * 1000).round}ms) Hey, #{YAML.load(File.open("data.yml"))[msg.user.id.to_s]}!"
  end

  #pr:version
  if msg.text.downcase.gsub(' ', '') == 'prb;version'
    testcmdInfo['version'] = {'description' => 'Shows the current bot version.', 'syntax' => 'prb;version'}
    msg.respond "Precipitation.rb #{testversion_name}"
  end

  #pr:help
  if msg.text.downcase.start_with? 'prb;help'
    testcmdInfo['help'] = {'description' => 'Gets a list of commands, or shows information about a command.', 'syntax' => 'prb;help (command)'}
    if msg.text.downcase.split()[1] == nil
      msg.message.reply "Current Commands: \n*~uptime \n~about \n~version \n~help \n~ping \n~pic \n~play-link \n~play-search*"
    else
      puts testcmdInfo
      puts testcmdInfo[msg.text.downcase.split()[1]]
      msg.channel.send_embed {|embed| embed.colour = embedcolour; embed.title = "Precipitation Index || prb;#{msg.text.downcase.split()[1]}"; embed.fields =  [Discordrb::Webhooks::EmbedField.new(name: 'Description', value: testcmdInfo[msg.text.downcase.split()[1]]['description']), Discordrb::Webhooks::EmbedField.new(name: 'Syntax', value: testcmdInfo[msg.text.downcase.split()[1]]['syntax'])]; embed.footer = Discordrb::Webhooks::EmbedFooter.new(text: "Precipitation.rb " + version + " || bolded is a required argument, () is an argument, [] is an option", icon_url: "https://cdn.discordapp.com/avatars/982908116163260416/ba2793e324adb306cb45e31a41ac5f9a.png?size=64")}
#">>> **__Precipitation Index || prb;#{msg.text.downcase.split()[1]}__** \n\n**Description** \n#{cmdInfo[msg.text.downcase.split()[1]]['description']} \n**Syntax** \n#{cmdInfo[msg.text.downcase.split()[1]]['syntax']}"
    end
  end

  #pr:pic
  if msg.text.downcase.split[0] == 'prb;pic'
    testcmdInfo['pic'] = {'description' => 'Gets the profile picture of yourself or another user.', 'syntax' => 'prb;pic (user)'}
    if msg.message.mentions[0] != nil
      msg.channel.send_embed {|embed| embed.colour = embedcolour; embed.title = (msg.message.mentions[0].name || msg.user.name) + "#" + (msg.message.mentions[0].discrim || msg.user.discrim) + "'s Profile Picture"; embed.footer = Discordrb::Webhooks::EmbedFooter.new(text: "Precipitation.rb " + version, icon_url: "https://cdn.discordapp.com/avatars/982908116163260416/ba2793e324adb306cb45e31a41ac5f9a.png?size=64"); embed.image = Discordrb::Webhooks::EmbedImage.new(url: (msg.message.mentions[0] || msg.user).avatar_url('jpg'));}
    else
      msg.channel.send_embed {|embed| embed.colour = embedcolour; embed.title = (msg.user.name) + "#" + (msg.user.discrim) + "'s Profile Picture"; embed.footer = Discordrb::Webhooks::EmbedFooter.new(text: "Precipitation.rb " + version, icon_url: "https://cdn.discordapp.com/avatars/982908116163260416/ba2793e324adb306cb45e31a41ac5f9a.png?size=64"); embed.image = Discordrb::Webhooks::EmbedImage.new(url: (msg.user).avatar_url('jpg'));}
    end
  end
}

#pr:fraud
testbot.message(start_with: "prb;deduction"){|msg|
  msg.respond 'Deducton isn\'t supported on Precipitation.rb-test!'
}
=end

testbot.ready(){
  testbot.playing= "#{version_name} || #{prefix}help"
}
testbot.message(){|msg|
    puts msg.user.name
    puts msg.text
    lastMsg = msg.message
}

Thread.new do
  while true do
   # bot.channel(987670662204252213).start_typing
    cmd = gets.chomp
    unless cmd.include?'--channel'
        begin
            lastMsg.respond cmd
        rescue NoMethodError
            puts "No message logged yet!"
        rescue Discordrb::Errors::MessageEmpty
            puts 'empty message'
        end
    else
        testbot.channel(cmd.split('--channel')[1]).send_message(cmd.split('--channel')[0])
    end
  end
end
testbot.run()
bot.run()
